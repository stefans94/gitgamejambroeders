using Assets.Scripts.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Scripts;


public class PlayerScript : MonoBehaviour
{
    public GameObject AppleSpawner;
    public GameObject Controls;
    public Transform Finish;
    public GameObject MainCamera;
    public AudioSource Music;

    //public GameObject AudioController;
    public float xMinus;
    public float Height;

    //public AudioClip ExplosionSound;

    //private AudioSource audioSource;

    private List<ActivePowerUp> powerUps;

    public GameObject DistanceText;
    private Text distanceText;

    public GameObject TimeText;
    private Text timeText;

    public List<GameObject> PowerUpIcons;
    public GameObject Panel;

    private GameObject Icon;
    private System.DateTime IconEnd;

    private bool Started = false;
    private float time = 0f;
    private float countDownNextLevel = 10f;
    private bool Finished = false;


    private void Start()
    {
        //audioSource = AudioController.GetComponent<AudioSource>();

        powerUps = new List<ActivePowerUp>();
        distanceText = DistanceText.GetComponent<Text>();
        timeText = TimeText.GetComponent<Text>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PowerUp>() != null)
        {
            Destroy(other.gameObject);

            other.gameObject.GetComponent<AudioSource>().Play();

            var rand = Random.Range(0, 0);

            switch (rand)
            {
                case 0:
                    var powerUp = new ActivePowerUp("Apples", System.DateTime.Now.AddSeconds(10));

                    ShowPowerUpIcon(0);

                    powerUp.Objects = new List<GameObject>();
                    powerUp.Objects.Add(Instantiate(AppleSpawner, new Vector3(transform.position.x, transform.position.y, transform.position.z), new Quaternion()));
                    Music.Stop();

                    powerUps.Add(powerUp);
                    
                    break;
            }
        }

        if (other.gameObject.tag == "Finish")
        {
            other.gameObject.GetComponent<AudioSource>().Play();
            Finished = true;
        }

        if (other.gameObject.tag == "Apple")
        {
            Destroy(other.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Colliding with " + collision.gameObject.tag);
        if (collision.gameObject.tag == "Floor" && collision.transform.position.y <= transform.position.y)
        {
            MainCamera.transform.position = new Vector3(MainCamera.transform.position.x, collision.transform.position.y + 10f, MainCamera.transform.position.z);
            Controls.transform.position = new Vector3(Controls.transform.position.x, collision.transform.position.y + .64f, Controls.transform.position.z);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && !Started)
        {
            Started = true;
            ScoreController.StartedLevel = true;
        }

        if (Input.GetKeyDown(KeyCode.Space) && Finished)
        {
            //ScoreController.SingleLevel = false;
            countDownNextLevel = 0f;
        }

        for (var j = powerUps.Count; j > 0; j--)
        {
            switch (powerUps[j - 1].Name)
            {
                case "Apples":
                    powerUps[j - 1].Objects[0].transform.position = transform.position; // + new Vector3(0f, 5f, 0f);
                    break;
            }

            if (powerUps[j - 1].EndTime < System.DateTime.Now)
            {
                if (powerUps[j-1].Name == "Apples")
                {
                    Music.Play();
                }

                if (powerUps[j - 1].Objects != null) { 
                    for (var i = powerUps[j - 1].Objects.Count; i > 0; i--)
                    {
                        var obj = powerUps[j - 1].Objects[i - 1];
                        powerUps[j - 1].Objects.Remove(obj);
                        Destroy(obj);
                    }
                }

                powerUps.Remove(powerUps[j - 1]);
            }

           
        }

        if (!Finished)
        {
            if (Started)
            {
                time += Time.deltaTime;
                timeText.text = (int)time + "s";
            }
            else
            {
                timeText.text = "0s";
            }

            var distance = Vector3.Distance(transform.position, Finish.position);

            distanceText.text = ((int)distance) + "m";
        } else
        {
            timeText.text = time.ToString("0.00") + "s";
            ScoreController.RegisterScore((float)System.Math.Round(time * 100f) / 100f);
            timeText.fontSize = 120;

            if (!ScoreController.SingleLevel)
            {
                distanceText.text = "Next level in " + (int)countDownNextLevel + " seconds..." + System.Environment.NewLine + "(or press Space to continue)";
            }
            else
            {
                distanceText.text = "Back to menu in " + (int)countDownNextLevel + " seconds..." + System.Environment.NewLine + "(or press Space to continue)";
            }
            

            countDownNextLevel -= Time.deltaTime;

            if (countDownNextLevel <= 0f)
            {
                var scene = SceneManager.GetActiveScene();
                Debug.Log((scene.buildIndex + 1) + " < " + SceneManager.sceneCount);
                if (!ScoreController.SingleLevel)
                { 
                    if (scene.buildIndex + 1 < SceneManager.sceneCountInBuildSettings)
                    {
                        SceneManager.LoadScene(scene.buildIndex + 1);
                    }
                }
                else
                {
                    SceneManager.LoadScene(0);
                }
                
            }
        }

        if (IconEnd < System.DateTime.Now) Destroy(Icon);
    }

    private void FixedUpdate()
    {
        //if (time != null) timeText.color = Random.ColorHSV();
    }

    private void ShowPowerUpIcon(int powerUp)
    {
        if (Icon != null) Destroy(Icon);
        Icon = Instantiate(PowerUpIcons[powerUp], Panel.transform);
        IconEnd = System.DateTime.Now.AddSeconds(5);
    }
}
