using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using UnityEngine.UI;
using System.Linq;

public class SheepLevelSetter : MonoBehaviour
{
    public int AmountofSheep;
    Text SheepUIText;
    Image SheepUIIcon;

    // Start is called before the first frame update
    void Start()
    {
        ScoreController.AmountSheepToFree = AmountofSheep;
        ScoreController.AmountSheepFreed = 0;
        SheepUIText = FindObjectsOfType<Text>().First(x => x.name == "SheepText");
        SheepUIIcon = FindObjectsOfType<Image>().First(x => x.name == "SheepIcon");
        var colortoset = SheepUIIcon.color;
        colortoset.a = 255f;
        SheepUIIcon.color = colortoset;
        colortoset = SheepUIText.color;
        colortoset.a = 255f;
        SheepUIText.color = colortoset;
        FindObjectsOfType<Text>().First(x => x.name == "DistanceText").color = new Color (0,0,0,0);
    }

    // Update is called once per frame
    void Update()
    {
        SheepUIText.text = ScoreController.AmountSheepFreed + " / " + ScoreController.AmountSheepToFree;

        if(ScoreController.AmountSheepToFree <= ScoreController.AmountSheepFreed)
        {
            var finishline = FindObjectsOfType<GameObject>().First(x => x.tag == "Finish");
            var horse = FindObjectsOfType<GameObject>().First(x => x.tag == "Player");

            finishline.transform.position = horse.transform.position;

        }
    }
}
