using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySelfInSeconds : MonoBehaviour
{
    public float Seconds = 1f;

    void Update()
    {
        Destroy(gameObject, Seconds);
    }
}
