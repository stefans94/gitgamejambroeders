using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenSpawner : MonoBehaviour
{
    public GameObject Chicken;
    public float SecondsBetweenChickens = 3f;
    public float ChickenSpeed = 10f;
    public float ChickenLifetime = 10f;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnChicken", SecondsBetweenChickens, SecondsBetweenChickens);
    }

    void SpawnChicken()
    {
        var newChicken = GameObject.Instantiate(Chicken);
        var chickenrotation = newChicken.transform.rotation;
        
        newChicken.transform.SetPositionAndRotation(new Vector3(transform.position.x, transform.position.y + 2f, transform.position.z), chickenrotation);// newChicken.transform.rotation);
        newChicken.GetComponent<ChickenScript>().Velocity = -gameObject.transform.forward * ChickenSpeed;
        newChicken.GetComponent<ChickenScript>().SpinChicken = true;
        newChicken.GetComponent<ChickenScript>().selfDestructInSeconds = ChickenLifetime;
    }
}
