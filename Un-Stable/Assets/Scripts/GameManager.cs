using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public bool Paused = false;
    public AudioSource Music;
    public float showEachHelpTextForSeconds = 3f;
    private float timeHelpTextShown = 0f;

    public Image HorseHelpTextBubbleGameObject;
    public Text HorseHelpTextGameObject;

    private int helpTextIndex = -1;
    private bool helpTextVisible = true;
    public List<String> NextHelpTextLines;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }

        if (Input.GetKeyUp(KeyCode.R))
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

        if (Input.GetKeyUp(KeyCode.M))
        {
            Music.mute = !Music.mute;
        }

        if (helpTextVisible)
        {
            timeHelpTextShown += Time.deltaTime;
            if(timeHelpTextShown >= showEachHelpTextForSeconds)
            {
                helpTextIndex++;
                timeHelpTextShown = 0;
                if(NextHelpTextLines.Count> helpTextIndex)
                {
                    //Er is nog meer helptext, toon volgende text

                    var formattedText =     "   " + NextHelpTextLines[helpTextIndex];
                    formattedText = formattedText.Replace(  "\\n", Environment.NewLine + "   ");
                    
                    HorseHelpTextGameObject.text = formattedText;
                }
                else
                {
                    HorseHelpTextBubbleGameObject.color = new Color(0,0,0,0);
                    HorseHelpTextGameObject.color = new Color(0, 0, 0, 0);
                    helpTextVisible = false;
                }
            }
        }
    }
}
