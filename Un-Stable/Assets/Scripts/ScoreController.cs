﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public static class ScoreController// : MonoBehaviour
    {
        public static int Score;
        public static float SecondsElapsed;
        public static int AmountApples;
        public static int AmountSheepToFree = 0;
        public static int AmountSheepFreed = 0;

        public static float Level1Seconds = -1;
        public static float Level2Seconds = -1;
        public static float Level3Seconds = -1;
        public static float Level4Seconds = -1;
        public static float Level5Seconds = -1;
        public static float Level6Seconds = -1;
        public static float Level7Seconds = -1;

        public static void RegisterScore(float seconds)
        {
            switch (SceneManager.GetActiveScene().buildIndex)
            {
                case 1:
                    if (seconds < Level1Seconds || Level1Seconds == -1)
                        Level1Seconds = seconds;
                    break;
                case 2:
                    if (seconds < Level2Seconds || Level2Seconds == -1)
                        Level2Seconds = seconds;
                    break;
                case 3:
                    if (seconds < Level3Seconds || Level3Seconds == -1)
                        Level3Seconds = seconds;
                    break;
                case 4:
                    if (seconds < Level4Seconds || Level4Seconds == -1)
                        Level4Seconds = seconds;
                    break;
                case 5:
                    if (seconds < Level5Seconds || Level5Seconds == -1)
                        Level5Seconds = seconds;
                    break;
                case 6:
                    if (seconds < Level6Seconds || Level6Seconds == -1)
                        Level6Seconds = seconds;
                    break;
                case 7:
                    if (seconds < Level7Seconds || Level7Seconds == -1)
                        Level7Seconds = seconds;
                    break;
                default:break;
            }
        }


        public static bool AllSheepCollected { get { return AmountSheepToFree <= AmountSheepFreed && AmountSheepToFree != 0; } }

        public static bool SingleLevel = false;
        public static bool StartedLevel = false;

        public static void CollectSheep()
        {
            Score += 100;
            AmountSheepFreed++;
        }

        public static void CollectApple()
        {
            Score += 1;
            AmountApples++;
        }

    }
}
