﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class ActivePowerUp
    {
        public string Name { get; set; }
        public DateTime EndTime { get; set; }

        public List<GameObject> Objects { get; set; }

        public ActivePowerUp(string name, DateTime endTime)
        {
            Name = name;
            EndTime = endTime;
        }
    }
}
