using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisUpdateScript : MonoBehaviour
{
 
    public float selfDestructInSeconds = 10f;

    // Start is called before the first frame update
    void Start()
    {    
    }

 
    public void Update()
    {
        if(selfDestructInSeconds > 0)
        {
            Destroy(gameObject, selfDestructInSeconds);
        }
    }
}
