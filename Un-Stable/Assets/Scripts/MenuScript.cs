using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Scripts;


public class MenuScript : MonoBehaviour
{


    private string sceneToLoad;
    public Animation buttonAnimation;


    public void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        if (ScoreController.SingleLevel)
        {
            GoToLevelSelect();
        }
        else
        {
            FindObjectsOfType<Transform>().First(x => x.name == "Buttons").GetComponent<Animator>().Play("ButtonsEntry");
            FindObjectsOfType<Transform>().First(x => x.name == "Logo").GetComponent<Animation>().Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void StartGame()
    {
        ScoreController.SingleLevel = false;
        SceneManager.LoadScene("Level0");

    }

    public static void LoadScene(string sceneName)
    {
        //sceneToLoad = sceneName;
        //StartCoroutine(FadeAndLoadScene(FadeDirection.In, sceneToLoad));
        ScoreController.SingleLevel = false;
        SceneManager.LoadScene(sceneName);
        
    }
    public static void LoadSceneFromLevelSelect(int sceneIndex)
    {
        //sceneToLoad = sceneName;
        //StartCoroutine(FadeAndLoadScene(FadeDirection.In, sceneToLoad));
        ScoreController.SingleLevel = true;
        SceneManager.LoadScene(sceneIndex);
        
    }

    public void GoToLevelSelect()
    {
        Color color;
        if (FindObjectsOfType<Text>(true).First(x => x.name == "TextButtonR").text == "Back to start")
        {
            FindObjectOfType<Camera>().GetComponent<Animator>().Play("PanToMainMenu");
            FindObjectsOfType<Text>(true).First(x => x.name == "TextButtonR").text = "Level Select";
            
            color = FindObjectsOfType<Text>(true).First(x => x.name == "TextButtonL").color;
            color.a = 255f;
            FindObjectsOfType<Text>(true).First(x => x.name == "TextButtonL").color = color;

            color = FindObjectsOfType<Image>(true).First(x => x.name == "ButtonL").color;
            color.a = 255f;
            FindObjectsOfType<Image>(true).First(x => x.name == "ButtonL").color = color;

            color = FindObjectsOfType<Image>(true).First(x => x.name == "Logo").color;
            color.a = 255f;
            FindObjectsOfType<Image>(true).First(x => x.name == "Logo").color = color;
        }
        else
        {
            if (ScoreController.SingleLevel)
            {
                FindObjectOfType<Camera>().GetComponent<Animator>().Play("JumpToLevelSelect");
            }
            else
            {
                FindObjectOfType<Camera>().GetComponent<Animator>().Play("PanToLevelSelect");
            }
            FindObjectsOfType<Text>(true).First(x => x.name == "TextButtonR").text = "Back to start";

            color = FindObjectsOfType<Text>(true).First(x => x.name == "TextButtonL").color;
            color.a = 0f;
            FindObjectsOfType<Text>(true).First(x => x.name == "TextButtonL").color = color;

            color = FindObjectsOfType<Image>(true).First(x => x.name == "ButtonL").color;
            color.a = 0f;
            FindObjectsOfType<Image>(true).First(x => x.name == "ButtonL").color = color;

            color = FindObjectsOfType<Image>(true).First(x => x.name == "Logo").color;
            color.a = 0f;
            FindObjectsOfType<Image>(true).First(x => x.name == "Logo").color = color;
        }

    }

}
