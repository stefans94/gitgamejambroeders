using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsController : MonoBehaviour
{
    public Transform Horse;
    public Transform AimController;
    public Transform BombSpawn;

    public GameObject Explosion;
    public Transform PowerBar;

    private bool increasePowerBar = true;
    private float power = 0;

    // Start is called before the first frame update
    void Start()
    {
        PowerBar.gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            PowerBar.gameObject.SetActive(true);
            if (increasePowerBar && power < .9f)
            {
                power += .03f;
            } else if (!increasePowerBar && power > 0f)
            {
                power -= .03f;
            } else
            {
                increasePowerBar = !increasePowerBar;
            } 
        }

        PowerBar.localScale = new Vector3(PowerBar.localScale.x, power, PowerBar.localScale.z);
    }

    // Update is called once per frame
    void Update()
    {

        //Get the Screen positions of the object
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);

        //Get the Screen position of the mouse
        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);

        //Get the angle between the points
        float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);

        //Ta Daaa
        AimController.rotation = Quaternion.Euler(new Vector3(0f, -angle, 0f) + new Vector3(0f, 90f, 0f));

        transform.position = new Vector3(Horse.position.x, transform.position.y, Horse.position.z);

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            //var explosion = Explosion;
            

            

            var explosion = Instantiate(Explosion, new Vector3(BombSpawn.position.x - (power * 2), Horse.position.y + 3f - (power * 2), BombSpawn.position.z), new Quaternion());
            if (power > 0)
            {
                explosion.GetComponent<Explosion>().height = (power / .9f) * 8f;
                //explosion.GetComponent<Explosion>().power = 3000 / (power * 10);
                //explosion.GetComponent<Explosion>().radius = 50 / (power * 10);
                //Debug.Log("Power: " + explosion.GetComponent<Explosion>().power);
                //Debug.Log("Radius: " + explosion.GetComponent<Explosion>().radius);
                //Debug.Log("Height: " + explosion.GetComponent<Explosion>().height);
            }

            /*var volume = audioSource.volume;
            audioSource.clip = ExplosionSound;
            audioSource.volume = 60f;
            audioSource.Play();
            audioSource.volume = volume;*/

            power = 0;
            increasePowerBar = true;
            PowerBar.gameObject.SetActive(false);

        }
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }
}
