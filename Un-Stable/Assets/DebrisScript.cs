using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisScript : MonoBehaviour
{
    public GameObject[] debris;
    public ParticleSystem ps = null;
    public float SecondsBetweenChickens = 3f;
    public float ChickenSpeed = 10f;
    public float sizemodmin = 1f;
    public float sizemodmax = 2f;
    public float ChickenLifetime = 3f;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnChicken", SecondsBetweenChickens, SecondsBetweenChickens);
    }

    void SpawnChicken()
    {
        int arraylength = debris.Length;

        Vector3 randomDirection = new Vector3(Random.value, Random.value, Random.value);
        var newChicken = GameObject.Instantiate(debris[Random.Range(0, arraylength)]);
        newChicken.transform.SetPositionAndRotation(new Vector3(transform.position.x, transform.position.y + 2f, transform.position.z), newChicken.transform.rotation);
        var sizemod = Random.Range(sizemodmin, sizemodmax);
        newChicken.transform.localScale = new Vector3(sizemod, sizemod, sizemod);
        newChicken.transform.Rotate(randomDirection);
        newChicken.GetComponent<Rigidbody>().velocity += new Vector3(Random.Range(-20,20), Random.Range(-20, 20), Random.Range(-20, 20));
        newChicken.AddComponent<DebrisUpdateScript>().selfDestructInSeconds=ChickenLifetime;


        var blep = Random.insideUnitCircle.normalized;
        
        if(ps != null)
        {
            ps.transform.position = new Vector3(newChicken.transform.lossyScale.x + 2, 0, 0);
            Instantiate(ps, newChicken.transform);
        }
        
   
    }
}

